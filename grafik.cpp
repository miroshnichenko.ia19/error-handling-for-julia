﻿/*Мирошниченко Юлия 108 группа
5 задание 20 вариант
Построить график зависимости координат от времени для шарика под действием силы
тяжести, помещенного в гладкую сферическую полость радиусом R*/


#include <iostream>
#include <windows.h>
#include "resource.h"
#include <math.h>
using namespace std;


int R;
int x0, z0,  X, Z;
double g, t, T, T1;
int vx0, vy0;
int d = 0;

class Vector
{
public:
	int* arr;
	int size;
public:
	Vector(int N = 0) :arr(0), size(0)
	{
		if (N > 0) arr = new int[N];
		if (arr) size = N;
	}

	Vector(const Vector& f) : arr(0), size(0)
	{
		if (f.size > 0) arr = new int[f.size];
		if (arr) {
			size = f.size;
			for (int i = 0; i < size; i++)
			{
				arr[i] = f.arr[i];
			}
		}
	}

	~Vector() { delete[] arr; }

	friend ostream& operator <<(ostream&, Vector&);
	friend istream& operator >>(istream&, Vector&);
};

ostream& operator<<(ostream& out, Vector& p)
{

	for (int i = 0; i < p.size; i++)
		out << p.arr[i] << " ";
	return out;
}

istream& operator >> (istream& in, Vector& p) {
	for (int i = 0; i < p.size; i++)
		in >> p.arr[i];
	return in;
}

Vector D(4);

void function()
{
	int x = D.arr[0];
	int z = D.arr[1];
	int x1, z1;
	x1 = x + vx0 * t;
	x1 = floorl(x1) + 1;
	z1 = z + vy0 * t;
	z1 = floorl(z1) + 1;
	vy0 = vy0 + g * t;
	vy0 = floorl(vy0) + 1;

	int a, b, h;
	a = (x1 - X) * (x1 - X);
	b = (z1 - Z) * (z1 - Z);
	h = sqrt(a + b);
	h = floorl(h) + 1;

	if (T1 >= T)
	{
		x1 = x;
		z1 = z;
		D.arr[2] = x1;
		D.arr[3] = z1;
		d = 1;
	}

	else if (h >= R)
	{
		x1 = x;
		z1 = z;
		
		/*
		int k = abs( ((x1 - x0) * (x1 - X) + (z1 - z0) * (z1 - Z))) / (sqrt((x1 - x0) ^ 2 + (z1 - z0) ^ 2) + sqrt((x1 - X) ^ 2 + (z1 - Z) ^ 2));
		vx0 = k * v;
		vy0 = sqrt(1 - k ^ 2) * v;
		vx0 = -vx0;
		*/
		double v = sqrt(vy0 ^ 2 + vx0 ^ 2);
		auto dz = h - a / (sqrt(h ^ 2 - a ^ 2));// tg
		double k = vy0 / v; //sin
		double f = atan(dz) - asin(k);
		vx0 = cos(f) * v;
		vy0 = sin(f) * v;
		vx0 = -vx0;


	
		D.arr[2] = x1;
		D.arr[3] = z1;
		
		T1 = T1 + t;
	}
	else
	{
		D.arr[2] = x1;
		D.arr[3] = z1;
		T1 = T1 + t;
	}
}

int WINAPI DlgProc(HWND hDlg, WORD wMsg, WORD wParam, DWORD)
{
	PAINTSTRUCT ps;

	if (wMsg == WM_CLOSE || wMsg == WM_COMMAND && wParam == IDOK) {
		EndDialog(hDlg, 0);
	}
	else
		if (wMsg == WM_INITDIALOG) {

			RECT rc;
			GetClientRect(hDlg, &rc);
			int dx = rc.right - rc.left;
			int dy = rc.bottom - rc.top;
		}

		else
			if (wMsg == WM_PAINT) {
				BeginPaint(hDlg, &ps);

				HPEN hPen = (HPEN)CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
				HPEN hOldPen = (HPEN)SelectObject(ps.hdc, hPen);


				POINT ptOld;
				Ellipse(ps.hdc, x0 - 1, z0 - 1 - R, (x0 - 1) + 2 * R, z0 - 1 + R); //круглая полость 


				while (d == 0)
				{
					function();
					MoveToEx(ps.hdc, D.arr[0], D.arr[1], &ptOld);
					LineTo(ps.hdc, D.arr[2], D.arr[3]);
					D.arr[0] = D.arr[2];
					D.arr[1] = D.arr[3];	
				}

				SelectObject(ps.hdc, hOldPen);
				DeleteObject(hPen);
				EndPaint(hDlg, &ps);
			}
	return 0;
}

int main()
{
	string command;
	bool menu = true;
	setlocale(LC_ALL, "rus");

	while (menu == true) {
		cout << "Вы будите вводить значения? Если да, то введите yes, если нет, то введите no " << endl;
		cin >> command;
		if (command == "yes") {
			cout << "Введите начальные координаты" << endl;
			cin >> x0 >> z0;
			cout << "Введите начальную скорость" << endl;
			cin >> vx0 >> vy0;
			cout << "Введите радиус полости" << endl;
			cin >> R;
			cout << "Введите время Т " << endl;
			cin >> T;
			cout << "x0 = " << x0 << "\ny0 = " << z0 << "\nvx0 = " << vx0 << "\nvy0 = "
				<< vy0 << "\nR = " << R << "\nT = " << T << endl;
			break;
		}
		else if (command == "no") {
			x0 = 100;
			z0 = 200;
			R = 150;
			T = 50;
			vx0 = 80;
			vy0 = 90;
			break;
		}
	}

		D.arr[0] = x0;
		D.arr[1] = z0;

		X = x0 - 1 + R;
		Z = z0 - 1;
		t = 0.05;
		g = 9.8;
		T1 = 0;


		DialogBox(NULL, MAKEINTRESOURCE(IDD_DIALOG1), NULL, (DLGPROC)DlgProc);

	}

